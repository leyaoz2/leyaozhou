// Put your js code here
let UICtrl = (function() {
    const elem = {
        Header: document.getElementsByClassName('Header')[0],
        Footer: document.getElementsByClassName('Footer')[0],
        Title: document.getElementsByClassName('Title')[0],
        Buttons: document.getElementsByClassName('Buttons')[0],
        HiddenRow: document.getElementsByClassName('RowHidden')[0],
        Rows: [...document.getElementsByClassName('Row')],
        SlidesContainer: document.querySelector('.slide_container'),
        Modal: document.getElementsByClassName('modal'),
        ModalContent: document.getElementsByClassName('modal_content'),
        Slides: document.getElementsByClassName('project'),
        leftArrow: document.getElementsByClassName('left')[0],
        rightArrow: document.getElementsByClassName('right')[0],
        exit: document.getElementsByClassName('modal_exit'),
        jojoArrow: document.querySelector('#jojo_arrow'),
    }

    window.onscroll = () => {
        selectedButton();
        shrinkHeader();
        flyInArrow();
    };

    window.onload = () => {

    };

    let selectedButton = function() {
        let pageTop = elem.Header.getBoundingClientRect().top + elem.Header.getBoundingClientRect().height;

        for (let row of elem.Rows) {
            // console.log(row);
            rowTop = row.getBoundingClientRect().top;
            rowBot = rowTop + row.getBoundingClientRect().height

            if (rowTop <= pageTop && rowBot >= pageTop) {
                document.getElementById(row.id + '_btn').style.borderBottom = '2px solid white';
            } else {
                document.getElementById(row.id + '_btn').style.borderBottom = null;
            }
        }
    };

    let shrinkHeader = function() {

        if (window.pageYOffset <= 10) {
            elem.Header.classList.remove('shrink');
            elem.Title.classList.remove('shrinkTitleFont');
            elem.Buttons.classList.remove('shrinkFont');
            elem.Title.classList.add('expandTitleFont');
            elem.Buttons.classList.add('expandFont');
            for (let btn of elem.Buttons.children) {
                btn.style.borderBottom = null;
            }
        } else {
            elem.Header.classList.add('shrink');
            elem.Title.classList.remove('expandTitleFont');
            elem.Buttons.classList.remove('expandFont');
            elem.Title.classList.add('shrinkTitleFont');
            elem.Buttons.classList.add('shrinkFont');
        }
    };

    let flyInArrow = function() {
        let curr_pos = elem.jojoArrow.getBoundingClientRect().top;
        let anime_start = true;

        if (curr_pos > elem.Header.getBoundingClientRect().top + window.innerHeight) {
            anime_start = true;
            elem.jojoArrow.classList.remove('end');
        } else {
            anime_start = false;
            elem.jojoArrow.classList.add('end');
        }
    }

    return {
        elements: elem,
    }
})();

let Ctrl = (function() {
    elem = UICtrl.elements;
    let slideIdx = 0;
    let slidesCount = 4;
    let modalIdx = -1;

    let scrollBtn = function() {
        for (let btn of elem.Buttons.children) {
            btn.addEventListener('click', (e) => {
                let target = document.getElementById(btn.id.slice(0, btn.id.length - 4));

                window.scroll({
                    top: target.offsetTop,
                    behavior: "smooth"
                })
            })
        }
    };

    let showSlide = function() {
        let angle = slideIdx / slidesCount * -360;

        elem.SlidesContainer.style.transform = `translateZ(-25rem) rotateY(${angle}deg)`;
    }

    let arrowBtn = function() {
        elem.leftArrow.addEventListener('click', () => {
            slideIdx -= 1;
            showSlide();
        });

        elem.rightArrow.addEventListener('click', () => {
            slideIdx += 1;
            showSlide();
        })
    }

    let modalBtn = function() {
        for (let mod_idx in elem.Modal) {
            if (mod_idx === "length") {
                return;
            }
            let mod = elem.Modal[Number(mod_idx)];
            mod.addEventListener('click', function() {
                try {

                    modalIdx = Number(mod_idx);

                    elem.SlidesContainer.removeChild(mod);
                    document.body.appendChild(mod);
                    mod.classList.add('modal_ver');
                    mod.children[0].classList.add('proj_modal');

                    let descendants = [...mod.getElementsByClassName("modal_content")];
                    for (let des of descendants) {
                        des.style.display = 'block';
                    }

                    let desc_title = [...mod.getElementsByClassName("title")];
                    for (let des of desc_title) {
                        des.classList.add('proj_expand');
                    }

                    let desc_icon = [...mod.getElementsByClassName("icon")]
                    for (let des of desc_icon) {
                        des.classList.add('proj_expand');
                    }

                    let desc_exit = [...mod.getElementsByClassName("modal_exit")];
                    for (let des of desc_exit) {
                        des.style.display = 'flex';
                    }

                    let icon = [...mod.getElementsByTagName("i")];

                    for (let i of icon) {
                        i.classList.remove('fa-5x');
                        i.classList.add('fa-3x');
                    }

                } catch (e) {

                }
            });

        }
    }

    let modalExitBtn = function() {
        for (let btn of elem.exit) {
            btn.addEventListener('click', (e) => {
                e.stopPropagation();
                let curr_modal = btn.parentElement.parentElement;

                document.body.removeChild(curr_modal);
                elem.SlidesContainer.insertBefore(curr_modal, elem.SlidesContainer.children[modalIdx]);
                curr_modal.classList.remove('modal_ver');
                curr_modal.children[0].classList.remove('proj_modal');

                let descendants = [...curr_modal.getElementsByClassName("modal_content")];
                for (let des of descendants) {
                    des.style.display = 'none';
                }

                let desc_title = [...curr_modal.getElementsByClassName("title")];
                for (let des of desc_title) {
                    des.classList.remove('proj_expand');
                }

                let desc_icon = [...curr_modal.getElementsByClassName("icon")];
                for (let des of desc_icon) {
                    des.classList.remove('proj_expand');
                }

                let desc_exit = [...curr_modal.getElementsByClassName("modal_exit")];
                for (let des of desc_exit) {
                    des.style.display = 'none';
                }

                let icon = [...curr_modal.getElementsByTagName("i")];
                for (let i of icon) {
                    i.classList.remove('fa-3x');
                    i.classList.add('fa-5x');
                }

            });
        }
    }

    return {
        init: () => {
            scrollBtn();
            arrowBtn();
            modalBtn();
            modalExitBtn();
        },

    }
})(UICtrl);

Ctrl.init();